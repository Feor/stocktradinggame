﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Windows.Forms;

namespace ClientPanel
{
    public partial class ClientForm : Form
    {
        internal static string UserName;
        internal static double Money;

        public ClientForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void ClientForm_Load(object sender, EventArgs e)
        {
            //http://127.0.0.1:17027/
            string[] lines = System.IO.File.ReadAllLines("server.txt");
            textBox2.Text = lines[0];
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //STGserv.UserServiceClient svc = new STGserv.UserServiceClient();//"", textBox2.Text);
            MainForm.svc.Endpoint.Address = new System.ServiceModel.EndpointAddress(textBox2.Text);// textBox2.Text;
            
            STGserv.UserDataType usr = new STGserv.UserDataType();
            usr.UserName = textBox1.Text;
            usr.Pass = maskedTextBox1.Text;
            usr.Money = 0;

            //svc.LoadService();
            //if (svc.get_gamestate() != "play") { svc.Close(); return; }
            MainForm.svc.Open();
            if (MainForm.svc.Login(usr))
            {
                //MainForm.svc.Close();
                MessageBox.Show("Login OK!", "Ok!");
                UserName = usr.UserName;
                Money = usr.Money;
                MainForm f = new MainForm();
                f.Show();
                f.Activate();
                this.Hide();
            }
            else
            {
                MainForm.svc.Close();
                MessageBox.Show("wrong user name or password", "not Ok!"); ;
                
            }



        }

     
    }
}
