﻿using ClientPanel.STGserv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ClientPanel
{
    public partial class MainForm : Form
    {
        static int perd=-1;
        //static TimeSpan per_time;
        static string username;
        static string time;
        static double money;
        static string game_state;
        public static UserServiceClient svc = new UserServiceClient();
        public MainForm()
        {
            InitializeComponent();
        }

        private void login_form_Load(object sender, EventArgs e)
        {
            ClientForm f = new ClientForm();
            label1.Text += ClientForm.UserName;
            label2.Text += Convert.ToString(ClientForm.Money);
           // List<int> items =int<100>;
            //for (int i = 0; i < 100; i++)//items[i]=i+1;
            //{
            //    domainUpDown1.Items.Add (i+1);
            //    domainUpDown2.Items.Add (i+1);
            //}
            //domainUpDown1.SelectedIndex=0;
            //domainUpDown2.SelectedIndex=0;
            username = ClientForm.UserName;

            //fresh_fields();
        }
        int lastindex1, lastindex2;
        private void fresh_fields()
        {
 
            string[] shrs;
            string[] prices;
            string[] companys;

            //string[] users;

            double[] mas1;
            double[] mas2;

            //UserServiceClient svc = new UserServiceClient();
           // svc.Open();
            if (svc.get_gamestate() != "play") { /*svc.Close();*/ return; }

            shrs = svc.get_shares(ClientForm.UserName);
            companys = svc.get_companys();
            prices = svc.get_prices();
            companys = svc.get_companys();
            label6.Text = "Game state:"+svc.get_gamestate();

           // svc.Close();  
          //  users = svc.



            lastindex1 = listBox1.SelectedIndex;
            lastindex2 = listBox2.SelectedIndex;

            listBox1.Items.Clear();
            for (int i = 0; i < companys.Length; i++)
            {
                listBox1.Items.Add(companys[i] + ":" + shrs[i]);
            }
            listBox1.SelectedIndex = lastindex1;
            listBox2.Items.Clear();
            for (int i = 0; i < companys.Length; i++)
            {
                listBox2.Items.Add(companys[i] + ":" + prices[i]);
            }
            listBox2.SelectedIndex = lastindex2;

            if (listBox1.SelectedIndex == -1) listBox1.SelectedIndex = 0;
            if (listBox2.SelectedIndex == -1) listBox2.SelectedIndex = 0;

            string s1 = listBox1.Items[listBox1.SelectedIndex].ToString();
            string s2 = listBox2.Items[listBox2.SelectedIndex].ToString();
            mas1 = svc.get_chart(s1.Substring(0, s1.LastIndexOf(':')));
            mas2 = svc.get_chart(s2.Substring(0, s2.LastIndexOf(':')));


            chart1.Series[0].Points.Clear();
            chart1.Series[0].Points.AddY(0);
            chart1.Series[0].Points.DataBindY(mas1);
            chart1.Titles[0].Text = listBox1.Items[listBox1.SelectedIndex > -1 ? listBox1.SelectedIndex : 0].ToString();

            chart2.Series[0].Points.AddY(0);
            chart2.Series[0].Points.Clear();
            chart2.Series[0].Points.DataBindY(mas2);
            chart2.Titles[0].Text = listBox2.Items[listBox2.SelectedIndex > -1 ? listBox2.SelectedIndex : 0].ToString();

            
        }


        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        //private void fresh_chart()
        //{ }
        private void refresher_Tick(object sender, EventArgs e)
        {
            
            //svc.Open();
            if (svc.get_gamestate() != "play") { /*svc.Close();*/ return; }
            if (svc.get_gamestate() == "play")
            {
                game_state = svc.get_gamestate();
                label6.Text = game_state;
                //label6.Text = svc.get_gamestate();
                //if (perd < svc.get_period()) fresh_chart(); ;

                perd = svc.get_period();
                label3.Text = "Period:" +(perd+1);
                //per_time = svc.get_period_time();
                time = svc.get_gametime();
                label4.Text = "Period time:"+time;//+ per_time.ToString();
                money = svc.get_my_money(username);
                label2.Text = money.ToString();
                textBox1.Lines = svc.get_winers();
                fresh_fields();
            }
            //svc.Close();
        }

        private void button1_Click(object sender, EventArgs e)//buy
        {
            if(label7.Text=="")return;
            if(textBox2.Text=="")return;
            //UserServiceClient svc = new UserServiceClient();
           
            double amount = Convert.ToDouble(textBox2.Text);
            string company = label7.Text;
            string user = ClientForm.UserName;

            //svc.Open();
            if (!svc.buy(user, company ,amount )) MessageBox.Show("not enough money", "error!"); 
            //svc.Close();

        }

        private void button2_Click(object sender, EventArgs e)//sell
        {
            if (label7.Text == "") return;
            if (textBox3.Text == "") return;
            //UserServiceClient svc = new UserServiceClient();
            //svc.Open();
            double amount = Convert.ToDouble(textBox3.Text);
            string company = label7.Text;
            string user = ClientForm.UserName;
            if (!svc.sell(user, company, amount)) MessageBox.Show("not enough shares", "error!"); 
            //svc.Close();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndex == -1) return;
            if (listBox2.SelectedIndex == lastindex2) return;
            string s = listBox2.Items[listBox2.SelectedIndex].ToString();
            label7.Text = s.Substring(0,s.LastIndexOf(':'));


        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1) return;
            if (listBox1.SelectedIndex == lastindex1) return;
            string s = listBox1.Items[listBox1.SelectedIndex].ToString();
            label7.Text = s.Substring(0, s.LastIndexOf(':'));

          

        }

        private void period_Tick(object sender, EventArgs e)
        {

        }

        private void chart2_Click(object sender, EventArgs e)
        {

        }
    }
}
