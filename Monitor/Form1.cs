﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monitor
{
    public partial class Monitor_form : Form
    {

        static string[] companys;
        static int period;
        static string time;
        STGGame.UserServiceClient svc = new STGGame.UserServiceClient();

        public Monitor_form()
        {
            InitializeComponent();
            //STGGame.UserServiceClient svc = new STGGame.UserServiceClient();
            string[] lines = System.IO.File.ReadAllLines("server.txt");
            //textBox2.Text = lines[0];
            svc.Endpoint.Address = new System.ServiceModel.EndpointAddress(lines[0]);
            svc.Open();
        }

        private void Period_timer_Tick(object sender, EventArgs e)
        {
            //STGGame.UserServiceClient svc = new STGGame.UserServiceClient();
           // svc.Open();
            time = svc.get_gametime();
            period = svc.get_period();
            //svc.Close();

            label1.Text = "Period: " + (period+1);
            label2.Text = "Period time: " + time;
        }

        private void Monitor_form_Load(object sender, EventArgs e)
        {
            companys = svc.get_companys();
            
            listBox1.Items.Clear();
            foreach (string company in companys) listBox1.Items.Add(company);
            listBox1.SelectedIndex = 0;
            //svc.Close();
        }

        private void Time_refresh_Tick(object sender, EventArgs e)
        {
            double[] chrt;
            if (listBox1.SelectedIndex == listBox1.Items.Count-1) listBox1.SelectedIndex = 0;else
            listBox1.SelectedIndex++;
            

           // STGGame.UserServiceClient svc = new STGGame.UserServiceClient();
            //svc.Open();
            string[] lines = System.IO.File.ReadAllLines("server.txt");
            svc.Endpoint.Address = new System.ServiceModel.EndpointAddress(lines[0]);

            if (svc.get_gamestate() != "play") { /*svc.Close();*/ return; }

            chrt = svc.get_chart(listBox1.SelectedItem.ToString());
            //svc.Close();

            chart1.Series[0].Points.Clear();
            chart1.Series[0].Points.DataBindY(chrt);
            chart1.Titles[0].Text = listBox1.SelectedItem.ToString();

            //for (int i = 0; i < chrt.Length; i++)
            //{
            //    int y = chrt[i];
            //    chart1.Series[0].Points.Add(y);
            //}
            //chart1.DataBind();

           // chart1.Series[0].Name = listBox1.SelectedItem.ToString();
           // for (int i = 0; i < chart.Length; i++ )
           // chart1.Series[0].Points.DataBindY(chrt);
                //chart1.Series[0].Points.Add(chrt[0]);
                //chart1.Series[0].Points.Add(chrt[1]);
                //chart1.Series[0].Points.Add(chrt[2]);
                //chart1.Series[0].Points.Add(chrt[3]);
                //chart1.Series[0].Points.Add(chrt[4]);
                //chart1.Series[0].Points.Add(chrt[5]);
                //chart1.Series[0].Points.Add(chrt[6]);
            //chart1.DataBind();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
