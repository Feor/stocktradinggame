# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* The Stock Trading Game
* Version 1.2
* Authtor: Feor_4: Beskhmelnitsky Anton, Russian Plekhanov University of Economics, FMEI(FI), Scince Society of Students
* 2013-2014

## How do I get set up? ##

### STG overview ###
Application consists of 4 components

* Server (ServerHost)
* Control Panel (ServerPanel)
* Monitor (Monitor)
* Game Client (ClientPanel)

Server should be runed on dedicited PC only! Control Panel shouldnt be viewed py players. Monitor should be vieweble for anyone. Game Clients need to be runed on team leaders PC's connected to the same network, that Server is started for. 

### Setup options ###

* Firstly set up User names in file userdata.txt (root folder of ServicePanel)

```
#!txt
Username1;password1
Username2;password2
Username3;password3
```

* Next step is to configurate server. Find in root folder of ServerHost next files:
    * NamesData.dat
    * PricesData.dat
    * UsersData.dat
    * SharesData.dat

* You should fill NamesData.dat with company names and PricesData.dat with prices for periods

* Next Step: run ServerHost (Administrator rights required)
* Next: run ServerPanel. Type in amount of money and hit Start NEW! Game
* Find server.txt in Monitor folder
* Fill it with servers URI
* Run Monitor
* Now you can run Clients. Usage of clients should be intuitive. You can change server.txt for automatic fill of server: field.

## Contribution guidelines ##

* 

## Who do I talk to? ##

* Admin: phenfeur@gmail.com