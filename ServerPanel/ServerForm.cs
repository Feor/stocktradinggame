﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ServerPanel
{
    public partial class ServerForm : Form
    {
        const int max_usr_count = 24;
        const int max_company_count = 20;
        const int max_period_count = 15;

        UserServiceClient svc = new UserServiceClient();

      static  DateTime Start_stump;
      static  int perd=0;
      static  string stime;
        public ServerForm()
        {
            InitializeComponent();
            svc.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "") return;


           // UserServiceClient svc = new UserServiceClient();

            UserDataType[] usrs = new UserDataType[max_usr_count];
            
          //  string s = "";
            string[] s = new string[2];
            char[] sep = { ';',':' };

            //svc.Open();

            for (int i = 0; i < textBox1.Lines.Count(); i++)
            {
                //s = "";
                //s = textBox1.Lines[i];
                UserDataType usr = new UserDataType();
                s = textBox1.Lines[i].Split(sep, 2, StringSplitOptions.RemoveEmptyEntries);

                usr.UserName = s[0];
                usr.Pass = s[1];
                usr.Money = Convert.ToDouble(textBox2.Text);

                usrs[i] = new UserDataType();

                usrs[i].UserName = usr.UserName;
                usrs[i].Pass = usr.Pass;
                usrs[i].Money = usr.Money;

              //  s[0] = "";
              //  s[1] = "";
              //  usr.UserName = "";
              //  usr.Pass = "";
              //  usr.Money = 0;
            }



            if (svc.SetUsersPasswords(usrs)) MessageBox.Show("everething OK! You can terminate Server", "OK!");
            else MessageBox.Show("Something is wrong!", "not OK!");

            //svc.LoadService();

           // svc.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           // UserServiceClient svc = new UserServiceClient();
            //svc.Open();

            svc.LoadService();

            //svc.Close();
        }

        private void ServerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
           string[] lines = textBox1.Lines;
           System.IO.File.WriteAllLines("userdata.txt", lines);
        }

        private void ServerForm_Load(object sender, EventArgs e)
        {
            string[] lines;
            lines = System.IO.File.ReadAllLines("userdata.txt");
            textBox1.Lines = lines;
        }


        private void button4_Click(object sender, EventArgs e)
        {
            
            //UserServiceClient svc = new UserServiceClient();
         //   svc.Open();
            

            Start_stump = DateTime.Now;

            timer_toString();

            //bool ok = svc.update(0,time,"play");

            svc.set_period(0);
            perd = 0;
            svc.set_state("play");


            svc.set_time(stime);

            Periodizer.Enabled = true;
            Updater.Enabled = true;

            //svc.Close();
        }

        private void timer_toString()
        {
            TimeSpan time = Start_stump.AddMinutes(7) - DateTime.Now;
            if(time.Minutes > 7)stime = "7:00";
            else stime = time.Minutes + ":" + time.Seconds;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //UserServiceClient svc = new UserServiceClient();
            //svc.Open();
            //bool ok = svc.update(perd,DateTime.Now- Start_stump,"play");
            timer_toString();
            svc.set_period(perd);
            svc.set_time(stime);
            textBox5.Lines = svc.get_winers();

            //svc.Close();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

            if (perd < max_period_count) perd++;
            if (perd == max_period_count) { perd = max_period_count-1; button6_Click(sender, e); };
            
            Start_stump = DateTime.Now;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //UserServiceClient svc = new UserServiceClient();
           // svc.Open();
           // bool ok = svc.update(perd, DateTime.Now - Start_stump, "stop");
            Updater.Stop();
            Periodizer.Stop();

            //Updater.Enabled = false;
           // Periodizer.Enabled = false;
            

            svc.set_state("stop");

           // svc.Close();
        }

        static int interval1;//, interval2;
        static DateTime paused_time;
        private void button5_Click(object sender, EventArgs e)
        {
            interval1 = Periodizer.Interval;
            Periodizer.Stop();
            paused_time = DateTime.Now;
            Updater.Stop();
           // UserServiceClient svc = new UserServiceClient();
           // svc.Open();
            svc.set_state("stop");
           // svc.Close();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Periodizer.Interval = interval1;
            Start_stump = Start_stump + (DateTime.Now - paused_time);
            Periodizer.Start();
            Updater.Start();


           // UserServiceClient svc = new UserServiceClient();
            //svc.Open();
            svc.set_state("play");
           // svc.Close();
        }

        private void button9_Click(object sender, EventArgs e)
        {
           // UserServiceClient svc = new UserServiceClient();
            //svc.Open();
            svc.add_money(textBox3.Text,(double)Convert.ToDouble(textBox4.Text));
           // svc.Close();
        }

        private void button10_Click(object sender, EventArgs e)
        {
           // UserServiceClient svc = new UserServiceClient();
           // svc.Open();


            Start_stump = DateTime.Now;

            timer_toString();

            //bool ok = svc.update(0,time,"play");

            svc.set_period(Convert.ToInt32(textBox6.Text)-1);
            perd = Convert.ToInt32(textBox6.Text)-1;
            svc.set_state("play");


            svc.set_time(stime);

            Periodizer.Enabled = true;
            Updater.Enabled = true;

           // svc.Close();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            //UserServiceClient svc = new UserServiceClient();
            //svc.Open();


            Start_stump = DateTime.Now;

            timer_toString();

            //bool ok = svc.update(0,time,"play");

            svc.set_period(max_period_count-1);
            perd = max_period_count-1;
            svc.set_state("play");


            svc.set_time(stime);

            Periodizer.Enabled = true;
            Updater.Enabled = true;

            //svc.Close();
        }
    }
}
