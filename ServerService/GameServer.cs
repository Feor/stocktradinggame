﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;



namespace ServerService
{
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service1" в коде и файле конфигурации.
    public class UserService : IUserService
    {
        const int max_usr_count = 24;
        const int max_company_count = 20;
        const int max_period_count = 15;

        static UserDataType[] users = new UserDataType[max_usr_count];
        static bool gameSet = false;
        //static bool timers = false;
        static string game_state = "stop";
        static string[] company_names = new string[max_company_count];//max_company_count строчек названий компаний, по одной на строчку
        static string[] company_prices = new string[max_company_count];//max_company_count строчек цен. по max_period_count цены в строке. по одной на компанию
        //static string[] company_news = new string[25];//24 строчки новостей по строчке новостей через сеп на период//о оно ваще надо?
        static string[] shares = new string[max_usr_count];//max_usr_count строчек. каждая строчка - max_period_count позиций double через сеп = владения юзера №i 

        static int game_period = 0;
        //static DateTime period_time;
        //static DateTime period_started;

        static string game_time;


        public void set_time(string time)
        {
            game_time = time;
        }
        public void set_period(int period) 
        {
            game_period = period;  
        }
        public void set_state(string state)
        {
            game_state = state;
        }

        //public bool update(int _period, TimeSpan _time, string _gamestate)
        //{
        //    game_period = _period;
        //    period_time = Convert.ToDateTime(_time);
        //    game_state = _gamestate;

        //    return true;
        //}
        public double[] get_chart(string company)
        {
            int comp_indx = max_company_count+1;
            char[] sep = {';'};
            string[] comp_prices;
            double[] reslt = new double[game_period+1];

            for (int j = 0; j < max_company_count; j++) if (company_names[j] == company) { comp_indx = j; break; };
            if (comp_indx != max_company_count+1)
            {
                comp_prices = company_prices[comp_indx].Split(sep, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < game_period+1; i++)
                    reslt[i] = Convert.ToDouble(comp_prices[i]);
            }

            return reslt;
        }

        //public int[] get_user_chart(string usr)
        //{
        //    int[] reslt ={0};
        //    return reslt;
        //}

        public string[] get_users()
        {
            string[] usors = new string[max_usr_count];

            for (int i = 0; i < max_usr_count; i++) usors[i] = users[i].UserName;

            return usors;
        }

        public void add_money(string user, double amount) 
        {
            for (int i = 0; i < max_usr_count; i++)
                if ((users[i].UserName == user )&& ( users[i].Money+amount>=0)) users[i].Money += amount;
        }
            
        public string[] get_winers()
        {
            string[] winers = new string[max_usr_count];
            double[] sum = new double[max_usr_count];///*= new double[10]*/ = {0,0,0,0,0,0,0,0,0,0};
            char[] sep={';'};
            string[][] shares_unpacked = new string[max_usr_count][];
            string[]  com_price ;


            for (int i = 0; i < max_usr_count; i++)
                {
                    shares_unpacked[i] = shares[i].Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    for (int j = 0; j < max_company_count; j++)
                    {
                      com_price = company_prices[j].Split(sep, StringSplitOptions.RemoveEmptyEntries);
                      sum[i] += Convert.ToDouble(shares_unpacked[i][j]) * Convert.ToDouble(com_price[game_period]);
                    }
                    //TODO::рассчет акций sum(распаковка( shares*company_prices));

                }

            for (int i = 0; i < max_usr_count; i++)
                {
                    double f = sum[i] + users[i].Money;
                    winers[i] = users[i].UserName + ':' + Convert.ToString(f);
                }

            return winers;
        }
        public double get_my_money(string user)
        {
            for (int i = 0; i < max_usr_count; i++)
                if (users[i].UserName == user) return users[i].Money; 
            return 0;

        }

        //public static TimeSpan Period_time
        //{
        //    get
        //    {
        //        var time = DateTime.Now - period_started;
        //        return time;
        //    }
        //    set {}
        //}

        //public struct company_data 
        //{//periods 1..24
        //   string[] company_names =new string[10];
        //   string[] company_prices = new string[10];

        //}
        //static company_data data = new company_data();
        public bool sell(string user, string company, double amount)
        {
            if (game_state != "play") return false;
            int usr_indx = max_usr_count+1;
            int comp_index = max_company_count+1;
            char[] sep={';'};

            for (int i = 0; i < max_usr_count; i++) if (users[i].UserName == user) { usr_indx = i; break; }
            for (int j = 0; j < max_company_count; j++) if (company_names[j] == company) { comp_index = j; break; };

            string[] usrs_shrs;
            if (usr_indx != max_usr_count+1) usrs_shrs = shares[usr_indx].Split(sep, StringSplitOptions.RemoveEmptyEntries);
            else return false;

            string[] comp_price;

            if (comp_index != max_company_count+1) comp_price = company_prices[comp_index].Split(sep, StringSplitOptions.RemoveEmptyEntries);
            else return false;

            if (Convert.ToDouble(usrs_shrs[comp_index]) >= amount)
            {
                users[usr_indx].Money += amount * Convert.ToDouble(comp_price[game_period]);
                usrs_shrs[comp_index] = Convert.ToString(Convert.ToDouble(usrs_shrs[comp_index]) - amount);
                string pack = "";
                foreach (string c in usrs_shrs) pack += c + ';';
                shares[usr_indx] = pack;
                return true;
            }
            else return false;
                

                
        } //поиск и операция
        public bool buy(string user, string company, double amount) {
            if (game_state != "play") return false;
            int usr_indx = max_usr_count+1;
            int comp_index = max_company_count+1;
            char[] sep = { ';' };

            for (int i = 0; i < max_usr_count; i++) if (users[i].UserName == user) { usr_indx = i; break; }
            for (int j = 0; j < max_usr_count; j++) if (company_names[j] == company) { comp_index = j; break; };

            string[] usrs_shrs;

            if (usr_indx != max_usr_count+1) usrs_shrs = shares[usr_indx].Split(sep, StringSplitOptions.RemoveEmptyEntries);//unpack
            else return false;

            string[] comp_price;

            if (comp_index != max_company_count+1) comp_price = company_prices[comp_index].Split(sep, StringSplitOptions.RemoveEmptyEntries);
            else return false;

            if (users[usr_indx].Money >= amount*Convert.ToDouble(comp_price[game_period]))
            {
                users[usr_indx].Money -= amount * Convert.ToDouble(comp_price[game_period]);
                usrs_shrs[comp_index] = Convert.ToString(Convert.ToDouble(usrs_shrs[comp_index]) + amount);
                string pack = "";
                foreach (string c in usrs_shrs) pack += c + ';';
                shares[usr_indx] = pack;
                return true;
            }
            else return false;} //поиск и операция
       // public string[] get_news() { return company_news; }
        public string[] get_companys() { return company_names; }
        public string[] get_prices() {
            string[,] unpack = new string[max_company_count, max_period_count];
            string[] result = new string[max_company_count];
            string[] line = new string[max_period_count];
            char[] sep={';'};

            for (int i = 0; i < max_company_count; i++)
             { 
                 line = company_prices[i].Split(sep,StringSplitOptions.RemoveEmptyEntries);
                 result [i]= line[game_period];
             }
 
            return result; }
        public string[] get_shares(string user) {

            int usr_indx = max_usr_count+1;
           
            char[] sep = { ';' };

            for (int i = 0; i < max_usr_count; i++) if (users[i].UserName == user) { usr_indx = i; break; }

            string[] usrs_shrs = new string[max_company_count];//={"","","","","","","","","",""};

            if (usr_indx != max_usr_count+1) usrs_shrs = shares[usr_indx].Split(sep, StringSplitOptions.RemoveEmptyEntries);
            
            return usrs_shrs; }
        public int get_period() { return game_period; }
       // public TimeSpan get_period_time() { return Period_time; }
        public string get_gametime() { return game_time; }
        public string get_gamestate() {return game_state;}
        public void load_operations()
        {
            company_names= System.IO.File.ReadAllLines("NamesData.dat");//каждая строчка название конторы
            company_prices = System.IO.File.ReadAllLines("PricesData.dat");// каждая строчка max_period_count значения цен по периодам через разделитель
            //company_news = System.IO.File.ReadAllLines("NewsData.dat");//24 строчки с новостями через сеп
            shares = System.IO.File.ReadAllLines("SharesData.dat");//на случай краша
        }
        public void LoadService()
        {
            StreamReader file = new StreamReader("UsersData.dat");
            string[] lines = System.IO.File.ReadAllLines("UsersData.dat");
            string[] bufr = new string[3];
            char[] sep = { ';',':'};
            int index = 0;

            foreach (string s in lines) 
            {
                bufr = s.Split(sep, 3, StringSplitOptions.RemoveEmptyEntries);
                
                users[index].UserName = bufr[0];
                users[index].Pass = bufr[1];
                users[index].Money = Convert.ToDouble(bufr[2]);
                Console.WriteLine(bufr[0] + " registred whith password: " + bufr[1] + " whith money: " + bufr[2]);
                if (index < lines.Length) index++; else return;
            }
            load_operations();
        }

        
         public bool Login(UserDataType usr) 
         {
             foreach (UserDataType C in users) if ((C.UserName == usr.UserName)&&(C.Pass==usr.Pass)) 
             {
                 Console.WriteLine("User {0} , with password {1} Loged in!", usr.UserName, usr.Pass);
                 return true; 
             }
             Console.WriteLine("User {0} , with password {1} Not Found!",usr.UserName,usr.Pass);
             return false;
         }
         public bool SetUsersPasswords(UserDataType[] UsrsData)
         {
             if (!UserService.GameSet)
             {
                 UserService.Users = UsrsData;

                 UserService.GameSet = true;

                 StreamWriter file = new StreamWriter("UsersData.dat", false);

                 for (int i = 0; i < max_usr_count; i++) file.WriteLine(UsrsData[i].UserName + ";" + UsrsData[i].Pass + ";" + UsrsData[i].Money);
                 file.Close();
                 LoadService();
                 Console.WriteLine("Game Seted! You Can Start Now =)!");
                 return true;
             }
             else
             {
                 Console.WriteLine("Game Already Seted! Restart Server!");
                 return false;
             }
         }
         internal static UserDataType[] Users
        {
            get { return UserService.users; }
            set { UserService.users = value; }
        }
         internal static bool GameSet
         {
             get { return UserService.gameSet; }
             set { if( !gameSet) UserService.gameSet = value; }
         }
    }

  
}
