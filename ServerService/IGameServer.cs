﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServerService
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        double[] get_chart(string company);
        //[OperationContract]
        //double[] get_user_chart(string usr);

        [OperationContract]
        string[] get_users();

        [OperationContract]
        void add_money(string user, double amount);//acept -


        [OperationContract]
        string[] get_winers();



//nedodelano =(
        //[OperationContract]
        //string[] get_news();


    
       // [OperationContract]
      //  TimeSpan get_period_time();
       
        //[OperationContract]
        //bool update(int _period, TimeSpan _time, string _gamestate);

        [OperationContract]
        void LoadService();//reset
        [OperationContract]
        bool Login(UserDataType usr);
        [OperationContract]
        bool SetUsersPasswords(UserDataType[] UsrsData);//10,10



        [OperationContract]
        string[] get_companys();
        [OperationContract]
        string[] get_prices();  
        [OperationContract]
        bool sell(string user,string company, double amount);
        [OperationContract]
        bool buy(string user, string company, double amount);
        [OperationContract]
        string[] get_shares(string user);
        [OperationContract]
        double get_my_money(string user);
//---------------------------------------------------------
        [OperationContract]
        string get_gametime();        
        [OperationContract]
        int get_period();
        [OperationContract]
        string get_gamestate();
//---------------------------------------------------------
        [OperationContract]
        void set_time(string time);
        [OperationContract]
        void set_period(int period);
        [OperationContract]
        void set_state(string state);

    }

   
    [DataContract]
    public class UserDataType
    {
        string userName;
        double  money;
        string pass;

        [DataMember]
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        [DataMember]
        public double Money
        {
            get { return money; }
            set { money = value; }
        }
        [DataMember]
        public string Pass
        {
            get { return pass; }
            set { pass = value; }
        }       
        
        public UserDataType()
        {
            userName = "";
            money = 0;
            pass = "";
        }
    }
}
